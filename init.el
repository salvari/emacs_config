;;----------------------------------------------------------------------
;; Package management
(require 'package)

(add-to-list 'package-archives
             '("melpa" . "http://melpa.org/packages/") t)

;; not needed anymore
;; (add-to-list 'package-archives
;;              '("gnu" . "http://elpa.gnu.org/packages/") t)

;; does not work
;;  (add-to-list 'package-archives '("marmalade" . "https://marmalade-repo.org/packages/") t)

;; not needed anymore
;; (add-to-list 'package-archives
;;              '("org" . "https://orgmode.org/elpa/") t)

;; disable automatic loading of packages after the init file
(setq package-initialize-at-startup nil)


;; load package explicitly
(package-initialize)


;; Change the user-emacs-directory to keep unwanted things out of ~/.emacs.d
(setq user-emacs-directory (expand-file-name "~/.cache/emacs/")
      url-history-file (expand-file-name "url/history" user-emacs-directory))

(setq custom-file (expand-file-name "custom.el" user-emacs-directory))


;; refresh package descriptions
(unless package-archive-contents
   (package-refresh-contents))


;; Set use-package install
(unless (package-installed-p 'use-package)
  (package-refresh-contents)
  (package-install 'use-package))


;; Use no-littering to automatically set common paths to the new user-emacs-directory
(use-package no-littering
  :ensure t)

;;------------------------------------------------------------
;; Load org-configuration file
(org-babel-load-file (expand-file-name "~/.emacs.d/myinit.org"))
